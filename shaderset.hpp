//
//  shaderset.hpp
//  render_design
//
//  Created by Chebonenko Vitali on 4/3/17.
//  Copyright © 2017 Chebonenko Vitali. All rights reserved.
//

#ifndef shaderset_hpp
#define shaderset_hpp


#include <stdio.h>
#include <iostream>


typedef unsigned int GLuint;
typedef unsigned int GLenum;

GLuint createShader(GLenum type, const char* fileName);
GLuint createShaderProgram(const GLuint &vertexShader, const GLuint &pixelShader);
bool checkShaderStatus(GLuint shaderID);
bool checkProgramStatus(GLuint programID);
std::string readShaderCode(const char* fileName);
void installShaders();

#endif /* shaderset_hpp */
