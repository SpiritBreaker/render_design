//
//  GLRender.hpp
//  render_design
//
//  Created by Chebonenko Vitali on 5/12/17.
//  Copyright © 2017 Chebonenko Vitali. All rights reserved.
//

#ifndef GLRender_hpp
#define GLRender_hpp

#include <stdio.h>

class Scene;

class GLRender
{
    Scene *scene;

public:
    
    GLRender(Scene &scene) :  scene(&scene)
    {
    };
    void render( int &width,  int &height);
    
};
#endif /* GLRender_hpp */
