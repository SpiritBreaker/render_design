//
//  Renderer.hpp
//  render_design
//
//  Created by Chebonenko Vitali on 3/14/17.
//  Copyright © 2017 Chebonenko Vitali. All rights reserved.
//

#ifndef Renderer_hpp
#define Renderer_hpp

#include <stdio.h>

class Renderer
{
    
protected:
    
public:
    
    virtual void Init() = 0;
    virtual void Resize(int width, int height) = 0;
	virtual int width() = 0;
	virtual int height() = 0;
    
};

#endif /* Renderer_hpp */
