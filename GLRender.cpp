//
//  GLRender.cpp
//  render_design
//
//  Created by Chebonenko Vitali on 5/12/17.
//  Copyright © 2017 Chebonenko Vitali. All rights reserved.
//

#include "GLRender.hpp"
#include <glm/gtc/matrix_transform.hpp>
#include "glm/gtx/string_cast.hpp"
#include "GL.hpp"
#include "Scene.hpp"

void GLRender::render( int &width,  int &height)
{
    glViewport(0, 0, width, height);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
    glClearColor(0, 0.2, 0.3, 1);
	glEnable(GL_MULTISAMPLE);
    
    const Camera* main_camera = &scene->Cameras.lookup(scene->MainCameraID);

	glm::mat4 viewToProjectionMatrix = glm::perspective(main_camera->FovY,
		main_camera->Aspect,
		main_camera->ZNear,
		main_camera->ZFar);

	glm::mat4 worldToViewMatrix = glm::lookAt(main_camera->position,
		main_camera->position + main_camera->target,
		main_camera->up);

    glm::mat4 Model = glm::mat4(1.0f); //* glm::scale(glm::mat4(1.0f), glm::vec3(0.01f));
	glm::mat4 worldToProjectionMatrix = viewToProjectionMatrix * worldToViewMatrix * Model;
    glUseProgram(scene->ShaderPrograms.lookup(1).ProgramID);
    GLuint matrix_handle = glGetUniformLocation(scene->ShaderPrograms.lookup(1).ProgramID, "worldToProjectionMatrix");
    glUniformMatrix4fv(matrix_handle, 1, GL_FALSE, &worldToProjectionMatrix[0][0]);
 //   
	//GLuint color_handle = glGetUniformLocation(scene->ShaderPrograms.lookup(1).ProgramID, "in_color");
	//glUniform1f(color_handle, 1.f);

	glActiveTexture(GL_TEXTURE0);
	

    for (auto &id: scene->loadedMeshIDs)
    {
        const Mesh* mesh = &scene->Meshes.lookup(id);
		const Material* material = &scene->Materials.lookup(mesh->MaterialID);
		glBindTexture(GL_TEXTURE_2D, material->TextureMapID);

        glBindVertexArray(mesh->MeshVAO);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->IndexBO);
        glDrawElements(GL_TRIANGLES, mesh->VertexCount, GL_UNSIGNED_INT, NULL);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
    }

	/*glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glEnable(GL_POLYGON_OFFSET_LINE);
	glPolygonOffset(-1, -1);
	float color_handle = 0.1f;
	glUniform1f(color_handle, 0.f);

	for (auto &id : scene->loadedMeshIDs)
	{
		const Mesh* mesh = &scene->Meshes.lookup(id);
		glBindVertexArray(mesh->MeshVAO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->IndexBO);
		glDrawElements(GL_TRIANGLES, mesh->VertexCount, GL_UNSIGNED_INT, NULL);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
	}
	glUniform1f(color_handle, 1.f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_POLYGON_OFFSET_LINE);*/

    glUseProgram(0);
}
