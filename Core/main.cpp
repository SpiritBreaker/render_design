//
//  VertexBuffer.cpp
//  render_design
//
//  Created by Chebonenko Vitali on 7/24/17.
//
//


#include "DataBuffer.hpp"
#include "lookup_table.hpp"
#include <iostream>
using namespace std;


class Test
{
public:
    void *data;
    size_t m_size;
    void alloc_data(size_t size, void *pData)
    {
        m_size = size;
        data = malloc(size);
        memcpy(pData, data, size);
    }

    
};


int main ()
{
    Test test;
    void *a;
    int b[5] = {1, 2, 3, 4, 5};
    
    a = malloc(sizeof(int) * 5);
    memcpy(a, b, sizeof(int)*5);
    
    cout << ((int*)a)[4] << endl;
    
    test.alloc_data(sizeof(int)*5, a);
    
    cout << ((int*)test.data)[4] << endl;
    
}
