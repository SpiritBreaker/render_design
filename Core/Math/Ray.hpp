#ifndef Ray_hpp
#define Ray_hpp

#include <stdio.h>
#include "glm/glm.hpp"
#include <glm/gtc/matrix_inverse.hpp>

glm::vec3 rayCastVector(
						const int &xpos,
						const int &ypos,
						const int &width, 
						const int &height,
						const glm::mat4 &viewToProjectionMatrix,
						const glm::mat4 &worldToViewMatrix
						)
{

	const glm::vec2 pos = glm::vec2(xpos, ypos);
	float xPoint = (2.0f * pos.x) / width - 1.0f;
	float yPoint = 1.0f - (2.0f * pos.y) / height;
	float zPoint = 1.0f;
	glm::vec3 ray_nds(xPoint, yPoint, zPoint);
	glm::vec4 ray_cip(ray_nds.x, ray_nds.y, -1.0, 1.0);
	glm::vec4 ray_eye = glm::inverse(viewToProjectionMatrix) *  ray_cip;
	ray_eye = glm::vec4(ray_eye.x, ray_eye.y, -1.0, 0.0);
	glm::vec4 ray_wor4 = glm::inverse(worldToViewMatrix) * ray_eye;
	glm::vec3 ray_wor = glm::vec3(ray_wor4.x, ray_wor4.y, ray_wor4.z);
	ray_wor = glm::normalize(ray_wor);

	printf("raycast vector: (%f, %f, %f);\n", ray_wor.x, ray_wor.y, ray_wor.z);
	return ray_wor;
}

#endif /* Ray_hpp */
