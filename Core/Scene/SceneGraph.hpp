//
//  scene.hpp
//  render_design
//
//  Created by Chebonenko Vitali on 3/10/17.
//  Copyright © 2017 Chebonenko Vitali. All rights reserved.
//

#ifndef scene_hpp
#define scene_hpp
#include <stdio.h>
#include <stdint.h>
#include <vector>
#include <string>
#include "lookup_table.hpp"
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

struct Transform
{
    glm::vec3 Scale;
    glm::vec3 RotationOrigin;
    glm::quat Rotation;
    glm::vec3 Translation;
};

struct Instance
{
    uint32_t MeshID;
    uint32_t TransformID;
};

struct Camera
{
    std::string name;
    glm::vec3 position;
    glm::vec3 target;
    glm::vec3 up;
    
    //Projection
    float FovY;
    float Aspect;
    float ZNear;
    float ZFar;
};

class Scene
{
public:
    
    lookup_table<Transform> Transforms;
    lookup_table<Instance> Instances;
    lookup_table<Camera> Cameras;
    
    uint32_t MainCameraID;
};

#endif /* scene_hpp */
