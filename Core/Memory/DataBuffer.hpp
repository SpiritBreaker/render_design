//
//  Buffer.hpp
//  render_design
//
//  Created by Chebonenko Vitali on 7/24/17.
//
//

#ifndef DataBuffer_hpp
#define DataBuffer_hpp

#include <stdio.h>

class DataBuffer
{
    
private:
    
    
    size_t mMemoryBudget;

    
public:
    
    void *data;
    size_t mSizeInBytes;
    
    DataBuffer(){};
    ~DataBuffer(){};
    
    enum Usage
    {
        STATIC =1,
        DYNAMIC = 1,
        WRITE_ONLY = 1
    };
    
    //virtual void readData(size_t offset, size_t length, void* pDest) = 0;
    //virtual void lockData();
    size_t getSizeInBytes(void) const { return mSizeInBytes; }
    void allocateData(size_t length, void* pDest);
    
protected:
    
};

#endif /* Buffer_hpp */
