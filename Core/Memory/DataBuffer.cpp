//
//  Buffer.cpp
//  render_design
//
//  Created by Chebonenko Vitali on 7/24/17.
//
//

#include <cstdlib>
#include <string.h>
#include "DataBuffer.hpp"


void DataBuffer::allocateData(size_t length, void *pDest)
{
    data = malloc(length);
    memcpy(data, pDest, length);
}
