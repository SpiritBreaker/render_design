# How to separate project to independent libraries 
# TODO write
# https://cognitivewaves.wordpress.com/cmake-and-visual-studio/


project(Memory_exe) #set project name
set(CMAKE_SUPPRESS_REGENERATION true) #Turn of generation of redundant projects in solution
set_property(GLOBAL PROPERTY USE_FOLDERS ON) #Turn on folder grouping in IDEs
set(CMAKE_CXX_STANDARD 11)

set  (MEMORY_SOURCES 
					 "DataBuffer.hpp"
					 "DataBuffer.cpp"

					 "IndexBuffer.hpp"
					 "IndexBuffer.cpp"

					 "PixelBuffer.hpp"
					 "PixelBuffer.cpp"

					 "VertexBuffer.hpp"
					 "VertexBuffer.cpp"

					 "lookup_table.hpp"
					 "lookup_table.cpp"
				   )

set (MEMORY_EXE_SOURCES "main.cpp")

include_directories(.)

add_library(Memory SHARED ${MEMORY_SOURCES})
add_executable(${PROJECT_NAME} ${MEMORY_EXE_SOURCES})
link_directories(${PROJECT_NAME} ${PROJECT_BINARY_DIR})
target_link_libraries(${PROJECT_NAME} Memory)
 
set_property(TARGET Memory PROPERTY FOLDER "Core/Memory" )
set_property(TARGET Memory_exe PROPERTY FOLDER "Core/Memory")

set_target_properties(Memory PROPERTIES RUNTIME_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR})

install(TARGETS Memory
	RUNTIME DESTINATION ${PROJECT_BINARY_DIR}/_install
	LIBRARY DESTINATION ${PROJECT_BINARY_DIR}/_install)

	#============================ POST BUILD PROPERTIES ======================================

		# reflect project hierarchy following folders structure
		foreach(source IN LISTS MEMORY_SOURCES)
    		get_filename_component(source_path "${source}" PATH)
    		string(REPLACE "/" "\\" source_path_msvc "${source_path}")
    		source_group("${source_path_msvc}" FILES "${source}")
		endforeach()

	#============================ POST BUILD PROPERTIES ======================================

		# reflect project hierarchy following folders structure
		foreach(source IN LISTS MEMORY_EXE_SOURCES)
    		get_filename_component(source_path "${source}" PATH)
    		string(REPLACE "/" "\\" source_path_msvc "${source_path}")
    		source_group("${source_path_msvc}" FILES "${source}")
		endforeach()

# enable_testing()
# add_test(libTest1 "main.cpp")