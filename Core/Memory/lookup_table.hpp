//
//  lookup_table.hpp
//  render_design
//
//  Created by Chebonenko Vitali on 3/10/17.
//  Copyright © 2017 Chebonenko Vitali. All rights reserved.
//

// This is the primitive implementation of ID look-up table
// more in depth implementation could be found here
// http://bitsquid.blogspot.com.by/2011/09/managing-decoupling-part-4-id-lookup.html
// the main limitation of the table it is limited by uint32_t;
//

// TO DO: make fast and flexible way to lookup by quering
// arbitrary object from the map by an attribute


#ifndef lookup_table_hpp
#define lookup_table_hpp


#include "lookup_table.hpp"
#include <map>
#include <memory>
#include <cstring>
#include <iostream>

//http://stackoverflow.com/questions/10452112/how-to-get-struct-member-with-a-string-using-macros-c
//http://stackoverflow.com/questions/20215129/passing-a-member-name-as-argument

template <class T>
class lookup_table
{
    
public:
    
    uint32_t _next_id;
    uint32_t terminate = 0; //zero value reserved
    std::map<uint32_t, T*> _objects;
    
    lookup_table() : _next_id(1)
    {
    }

    inline bool has(uint32_t id)
    {
        return _objects.count(id) > 0;
    }
    
    inline T &lookup(uint32_t id){
        return *_objects[id];
    }
    
    template <typename MembType, typename Value>
    inline const uint32_t &query(MembType memb, Value value)
    {
        for(auto &iter : _objects)
        {
            if (iter.second->*memb == value)
                return iter.first;
        }
        return terminate;
    }
    
    inline uint32_t add(T *obj)
    {
        uint32_t id = _next_id++;
        _objects[id] = obj;
        return id;
    }
    
    inline void remove(uint32_t id)
    {
        T &o = lookup(id);
        _objects.erase(id);
        delete &o;
    }
};

#endif /* lookup_table_hpp */
