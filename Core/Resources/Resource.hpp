//
//  Resource.hpp
//  render_design
//
//  Created by Chebonenko Vitali on 7/24/17.
//
//
// A game engine’s runtime resource manager takes on a wide range of respon- sibilities,
// all related to its primary mandate of loading resources into memory:
//
//  - Ensures that only one copy of each unique resource exists in memory at any given time.
//
//  - Manages the lifetime of each resource.
//
//  - Loads needed resources and unloads resources that are no longer needed.
//
//  - Handles loading of composite resources. A composite resource is a resource
//    comprised of other resources. For example, a 3D model is a composite resou-
//    rce that consists of a mesh, one or more materials, one or more textures
//    and optionally a skeleton and multiple skeletal animations.
//
//  - Maintains referential integrity. This includes internal referential integrity
//    (cross-references within a single resource) and external referential integrity
//    (cross-references between resources). For example, a model refers to its mesh
//    and skeleton; a mesh refers to its materials, which in turn refer to texture
//    resources; animations refer to a skeleton, which ultimately ties them to one or
//    more models. When loading a composite resource, the resource manager must ensure
//    that all necessary subresources are loaded, and it must patch in all of the
//    cross-references properly.
//
//  - Manages the memory usage of loaded resources and ensures that resources are
//    stored in the appropriate place(s) in memory.
//
//  - Permits custom processing to be performed on a resource after it has been loaded,
//    on a per-resource-type basis. This process is sometimes known as logging in or
//    load-initializing the resource.
//
//  - Usually (but not always) provides a single unified interface through which a wide
//    variety of resource types can be managed. Ideally a resource manager is also easily
//    extensible, so that it can handle new types of re- sources as they are needed by
//    the game development team.
//
//  - Handles streaming (i.e., asynchronous resource loading), if the engine supports this feature.
//

#ifndef Resource_hpp
#define Resource_hpp

#include <stdio.h>

class  Resource
{
    
private:

public:
    
};

#endif /* Resource_hpp */
