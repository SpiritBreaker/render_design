//
//  Texture.hpp
//  render_design
//
//  Created by Chebonenko Vitali on 7/24/17.
//
//

#ifndef Texture_hpp
#define Texture_hpp

#include <stdio.h>
#include <stdint.h>
#include <iostream>
#include "Resource.hpp"
#include "PixelBuffer.hpp"
#include <iostream>

using namespace std;


class Texture : public Resource
{
    
private:
    
public:
    
    std::string Name;
    
    Texture(){};
    ~Texture(){};
    
    PixelBuffer texture;
    

    
};


#endif /* Texture_hpp */
