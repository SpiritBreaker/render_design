//
//  Texture.cpp
//  render_design
//
//  Created by Chebonenko Vitali on 7/24/17.
//
//
#define TINYOBJLOADER_IMPLEMENTATION
#define	STB_IMAGE_IMPLEMENTATION

#include "tiny_obj_loader.h"
#include "log.h"
#include "ResourceManager.hpp"
#include "DataBuffer.hpp"
#include "Mesh.hpp"
#include "Material.hpp"
#include <vector>
#include <fstream>



uint32_t ResourceManager::load_mesh(const std::string& filename)
{
    std::vector<tinyobj::shape_t> shapes;
    std::vector<tinyobj::material_t> materials;
    std::string err;
    
    // assume mtl is in the same folder as the obj
    std::string mtl_basepath = filename;
    size_t last_slash = mtl_basepath.find_last_of("/");
    if (last_slash == std::string::npos)
        mtl_basepath = "./";
    else
        mtl_basepath = mtl_basepath.substr(0, last_slash + 1);
    
    if (!tinyobj::LoadObj(shapes, materials, err,
                          filename.c_str(), mtl_basepath.c_str(),
                          tinyobj::triangulation | tinyobj::calculate_normals))
    {
        fprintf(stderr, "Failed to load: %s\n", err.c_str());
    }

    for (const auto& shape: shapes)
    {
        Mesh *newMesh = new Mesh();
        newMesh->Name = shape.name;
        newMesh->IndexCount = shape.mesh.indices.size();
        newMesh->VertexCount = shape.mesh.positions.size();
    
//        newMesh->VerticesBO.allocateData( shape.mesh.positions.size() * sizeof(float),
//                                         shape.mesh.positions.data());
//        newMesh->TexCoordBO.allocateData(shape.mesh.texcoords.size() * sizeof(float),
//                                         shape.mesh.texcoords.data());
//        newMesh->NormalsBO.allocateData(shape.mesh.normals.size() * sizeof(float),
//                                        shape.mesh.normals.data());
//        newMesh->IndicesBO.allocateData(shape.mesh.indices.size() * sizeof(float),
//                                        shape.mesh.indices.data());
//    
        //Add mesh to database
        uint32_t meshId = Meshes.add(newMesh);
        loadedMeshIDs.push_back(meshId);
    }
}

uint32_t ResourceManager::create_mesh(const std::string& name,
                                  const std::vector<float>& positions,
                                  const std::vector<float>& texcoords,
                                  const std::vector<float>& normals,
                                  const std::vector<unsigned int>& indices)
{
    Mesh *newMesh = new Mesh();
    newMesh->Name = name;
    newMesh->IndexCount = indices.size();
    newMesh->VertexCount = positions.size();
    
//    newMesh->VerticesBO.allocateData( positions.size() * sizeof(float),
//                                     positions.data());
//    newMesh->TexCoordBO.allocateData(texcoords.size() * sizeof(float),
//                                     texcoords.data());
//    newMesh->NormalsBO.allocateData(normals.size() * sizeof(float),
//                                    normals.data());
//    newMesh->IndicesBO.allocateData(indices.size()* sizeof(float),
//                                    indices.data());
//    
    //Add mesh to database
    uint32_t meshId = Meshes.add(newMesh);
    loadedMeshIDs.push_back(meshId);
}

uint32_t ResourceManager::load_texture(const std::string& filename)
{
    
}

uint32_t ResourceManager::create_texture(const int &width, const int &height, const void* data)
{
    
}

std::vector<uint32_t> ResourceManager::load_material(const std::string& filename)
{
    std::vector<uint32_t> loadedIDs;
    
    std::map<std::string, int> material_map;
    std::vector<tinyobj::material_t> materials;
    
    std::filebuf fb;
    if (fb.open(filename.c_str(), std::ios::in))
    {
        std::istream is(&fb);
        tinyobj::LoadMtl(material_map, materials, is);
        fb.close();
    }
    
    //assume that textures are in the same folder where mtl file
    std::string path_to_mtl = filename;
    path_to_mtl.erase(path_to_mtl.find_last_of("/"), path_to_mtl.length());
    
    for (auto material : materials)
    {
        if (Materials.query(&Material::Name, material.name))
        {
            FILE_LOG(logINFO) << material.name  << " already exist. Skipped";
            continue;
        }
        
        Material* newMaterial = new Material();
        newMaterial->Name = material.name;
        newMaterial->TextureMapID = load_texture(path_to_mtl + "/" + material.diffuse_texname);
        //newMaterial->NormalMapID = loadTexture(scene, material.bump_texname);
        uint32_t materialID = Materials.add(newMaterial);
        loadedMaterialIDs.push_back(materialID);
        loadedIDs.push_back(materialID);
    }
    
    return loadedIDs;
    
}

uint32_t ResourceManager::create_material()
{
    
}

