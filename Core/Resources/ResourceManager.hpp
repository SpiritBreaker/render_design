//
//  Texture.hpp
//  render_design
//
//  Created by Chebonenko Vitali on 7/24/17.
//
//

#ifndef ResourceManager_hpp
#define ResourceManager_hpp

#include "lookup_table.hpp"
#include <stdio.h>
#include <stdint.h>
#include <iostream>
#include <vector>

using namespace std;


class Texture;
class Material;
class Mesh;

class ResourceManager
{
    
private:
    
    std::vector<uint32_t> loadedMeshIDs;
    std::vector<uint32_t> loadedMaterialIDs;
    std::vector<uint32_t> loadedTextureIDs;
    
    lookup_table<Texture> Textures;
    lookup_table<Material> Materials;
    lookup_table<Mesh> Meshes;
    
public:
    
    //The ability to deal with multiple types of resources
    
    //The ability to create new resource
    std::vector<uint32_t> load_material(const std::string& filename);
    uint32_t create_material();
    
    uint32_t load_mesh(const std::string& filename);
    uint32_t create_mesh(const std::string& name,
                     const std::vector<float>& positions,
                     const std::vector<float>& texcoords,
                     const std::vector<float>& normals,
                     const std::vector<unsigned int>& indices);
    
    uint32_t load_texture(const std::string& filename);
    uint32_t create_texture(const int &width, const int &height, const void* data);
    
    //The ability to delete resources
    
    //The ability to inspect and modify existing resources
    uint32_t get_by_name(){};
    
    //The ability to move a resource's source file(s) from one location to another on-disk.
    
    //The ability of a resources to cross-reference other resources.
    
};


#endif /* Texture_hpp */
