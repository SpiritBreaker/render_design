//
//  Material.hpp
//  render_design
//
//  Created by Chebonenko Vitali on 7/24/17.
//
//

#ifndef Material_hpp
#define Material_hpp

#include <stdio.h>

#include <stdio.h>
#include <stdint.h>
#include <iostream>
#include "Resource.hpp"

using namespace std;


class Material : public Resource
{
    
private:
    
    
public:
    
    Material(){};
    ~Material(){};
    
    std::string Name;
    float Ambient[3];
    float Diffuse[3];
    float Scpecular[3];
    
    float Shiness;
    uint32_t TextureMapID;
    uint32_t NormalMapID;
    

    
    
};

#endif /* Material_hpp */
