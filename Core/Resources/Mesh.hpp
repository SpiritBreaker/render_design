//
//  Mesh.hpp
//  render_design
//
//  Created by Chebonenko Vitali on 7/24/17.
//
//

#ifndef Mesh_hpp
#define Mesh_hpp

#include <stdio.h>
#include <vector>
#include "Resource.hpp"
#include <stdint.h>
#include "DataBuffer.hpp"
#include <iostream>

using namespace std;

class Scene;

class Mesh : public Resource
{
    
private:
    
public:
    
    std::string Name;
    
    Mesh(){};
    ~Mesh(){};
    
    DataBuffer VerticesBO;
    DataBuffer IndicesBO;
    DataBuffer NormalsBO;
    DataBuffer TexCoordBO;

    uint32_t MaterialID;
    uint32_t IndexCount;
    uint32_t VertexCount;
    
    //implement AxisAlignedBBox mAABB;
    
    

};


#endif /* Mesh_hpp */
