# How to separate project to independent libraries 
# TODO write
# https://cognitivewaves.wordpress.com/cmake-and-visual-studio/


project(Core_exe) #set project name
set(CMAKE_SUPPRESS_REGENERATION true) #Turn of generation of redundant projects in solution
set_property(GLOBAL PROPERTY USE_FOLDERS ON) #Turn on folder grouping in IDEs
set(CMAKE_CXX_STANDARD 11)

set  (CORE_SOURCES 
							"Scene/SceneGraph.hpp"
							"Scene/SceneGraph.cpp"

							"Math/Ray.cpp"
							"Math/Ray.hpp"

							"Memory/DataBuffer.hpp"
							"Memory/DataBuffer.cpp"
							"Memory/IndexBuffer.hpp" 
							"Memory/IndexBuffer.cpp"
							"Memory/lookup_table.hpp" 
							"Memory/lookup_table.cpp"
							"Memory/PixelBuffer.hpp" 
							"Memory/PixelBuffer.cpp"
							"Memory/VertexBuffer.hpp"
							"Memory/VertexBuffer.cpp" 

							"Resources/Material.cpp"
							"Resources/Material.hpp"
							"Resources/Mesh.cpp"
							"Resources/Mesh.hpp"
							"Resources/Texture.hpp"			
							"Resources/Texture.cpp"			
							"Resources/Resource.hpp"			
							"Resources/Resource.cpp"
							"Resources/ResourceManager.hpp"
							"Resources/ResourceManager.cpp"

				   )

set (CORE_EXE_SOURCES "main.cpp")

include_directories(${PROJECT_SOURCE_DIR}
					${PROJECT_SOURCE_DIR}/Scene
					${PROJECT_SOURCE_DIR}/Math	
					${PROJECT_SOURCE_DIR}/Memory
					${PROJECT_SOURCE_DIR}/Resources		
					${CODE_BASE_PATH}/include)

add_library(Core SHARED ${CORE_SOURCES})
add_executable(${PROJECT_NAME} ${CORE_EXE_SOURCES})
link_directories(${PROJECT_NAME} ${PROJECT_BINARY_DIR})
target_link_libraries(${PROJECT_NAME} Core)
 
set_property(TARGET Core PROPERTY FOLDER "Core" )
set_property(TARGET Core_exe PROPERTY FOLDER "Core")

set_target_properties(Core PROPERTIES RUNTIME_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR})

install(TARGETS Core
	RUNTIME DESTINATION ${PROJECT_BINARY_DIR}/_install
	LIBRARY DESTINATION ${PROJECT_BINARY_DIR}/_install)

	#============================ POST BUILD PROPERTIES ======================================

		# reflect project hierarchy following folders structure
		foreach(source IN LISTS CORE_SOURCES)
    		get_filename_component(source_path "${source}" PATH)
    		string(REPLACE "/" "\\" source_path_msvc "${source_path}")
    		source_group("${source_path_msvc}" FILES "${source}")
		endforeach()

	#============================ POST BUILD PROPERTIES ======================================

		# reflect project hierarchy following folders structure
		foreach(source IN LISTS CORE_EXE_SOURCES)
    		get_filename_component(source_path "${source}" PATH)
    		string(REPLACE "/" "\\" source_path_msvc "${source_path}")
    		source_group("${source_path_msvc}" FILES "${source}")
		endforeach()

# enable_testing()
# add_test(libTest1 "main.cpp")