#version 410

in vec2 TexCoord;

uniform sampler2D sampler;
uniform float in_color = 1.f;

out vec4 color;

void main()
{
	vec4 texColor = texture(sampler, TexCoord);
	color = texColor;
	//color = vec4(in_color, in_color, in_color, 1.0f);
}

