#version 410

layout (location = 0) in vec3 pos;
layout (location = 1) in vec2 UV;
layout (location = 2) in vec3 normals;

out vec2 TexCoord;

uniform mat4 worldToProjectionMatrix;

void main()
{
	TexCoord = UV;
    gl_Position =  worldToProjectionMatrix * vec4(pos, 1.0f);
}

