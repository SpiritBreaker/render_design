//
//  CameraRig.cpp
//  render_design
//
//  Created by Chebonenko Vitali on 3/22/17.
//  Copyright © 2017 Chebonenko Vitali. All rights reserved.
//
// https://learnopengl.com/#!Getting-started/Camera


#include "PhysicalCameraRig.hpp"
#include "Renderer.hpp"
#include "scene.hpp"
#include "log.h"
#include "GLFWAdapter.hpp"
#include <glm/gtc/matrix_transform.hpp>
#include "Ray.hpp"
#include "GLFWInput.hpp"
#include "GLFWRenderer.hpp"
#include "glm/gtx/string_cast.hpp"

PhysicalCameraRig::PhysicalCameraRig(Camera &camera, Renderer &renderer) : m_camera(&camera), m_renderer(&renderer)
{
	velocity_vector = m_camera->target;
};

void PhysicalCameraRig::setUpCamera(Camera &camera)
{
	m_camera = &camera;
}

void PhysicalCameraRig::move_forward()
{
	velocity_vector = glm::normalize(velocity_vector + m_camera->target);
}

void PhysicalCameraRig::move_backward()
{
	velocity_vector = glm::normalize(velocity_vector - m_camera->target);
}

void PhysicalCameraRig::strafe_right()
{
	velocity_vector = glm::normalize(velocity_vector + glm::normalize(glm::cross(m_camera->target, m_camera->up)));
}

void PhysicalCameraRig::strafe_left()
{
	velocity_vector = glm::normalize(velocity_vector - glm::normalize(glm::cross(m_camera->target, m_camera->up)));
}


void PhysicalCameraRig::mouseMoveEvent(double xpos, double ypos)
{

	if (firstMouse) // this bool variable is initially set to true
	{

		lastX = xpos;
		lastY = ypos;
		firstMouse = false;
	}

	float xoffset = xpos - lastX;
	float yoffset = lastY - ypos; // Reversed since y-coordinates range from bottom to top
	lastX = xpos;
	lastY = ypos;

	float sensitivity = 0.05f;
	xoffset *= sensitivity;
	yoffset *= sensitivity;

	yaw += xoffset;
	pitch += yoffset;

	if (pitch > 89.0f)
		pitch = 89.0f;
	if (pitch < -89.0f)
		pitch = -89.0f;


	glm::vec3 front;
	front.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
	front.y = sin(glm::radians(pitch));
	front.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
	m_camera->target = glm::normalize(front);


	//std::cout << glm::to_string(m_camera->front) << std::endl;


}

void PhysicalCameraRig::set_default_position()
{
    m_camera->position = glm::vec3(0.f, 0.f, 3.f);
    m_camera->target = glm::vec3(0.f, 0.f, -1.f);
    m_camera->up = glm::vec3(0.f, 1.f, 0.f);
}

void PhysicalCameraRig::mouseButtonEvent(int button, int action, int mods)
{

}

void PhysicalCameraRig::keyEvent(int key, int scancode, int action, int mods)
{
	switch (key)
	{
	case KEY_W:
		this->move_forward();
		break;

	case KEY_S:
		this->move_backward();
		break;

	case KEY_A:
		this->strafe_left();
		break;

	case KEY_D:
		this->strafe_right();
		break;

	}
}


void PhysicalCameraRig::do_movements()
{

	if (GLFWInput::keys[KEY_W] ||
		GLFWInput::keys[KEY_S] ||
		GLFWInput::keys[KEY_A] ||
		GLFWInput::keys[KEY_D])
	{
		camera_speed = camera_speed + accel_rate * 2 * GLFWRenderer::deltaTime;
		if (camera_speed >= max_speed)
			camera_speed = max_speed;
	}


	if (GLFWInput::keys[KEY_W])
		this->move_forward();

	if (GLFWInput::keys[KEY_S])
		this->move_backward();

	if (GLFWInput::keys[KEY_A])
		this->strafe_left();

	if (GLFWInput::keys[KEY_D])
		this->strafe_right();
        
    if (GLFWInput::keys [KEY_F])
        this->set_default_position();
    


	glm::vec3 f_traction = m_camera->target * 10.f;
	glm::vec3 f_drag = -10.f * velocity_vector * glm::length(velocity_vector);
	glm::vec3 f_long = f_traction + f_drag;


	m_camera->position += camera_speed * velocity_vector;

	if (camera_speed > 0.0f)
	{
		camera_speed = camera_speed - accel_rate * GLFWRenderer::deltaTime;
	}
	if (camera_speed < 0.0f)
	{
		camera_speed = 0.0f;
	}

	
}
