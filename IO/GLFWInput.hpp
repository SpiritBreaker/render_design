//
//  GLFWInputDevice.hpp
//  render_design
//
//  Created by Chebonenko Vitali on 4/7/17.
//  Copyright © 2017 Chebonenko Vitali. All rights reserved.
//

#ifndef GLFWInput_hpp
#define GLFWInput_hpp

#include <stdio.h>
#include "log.h"
#include "Signal.hpp"


struct GLFWwindow;

class GLFWInput
{
    GLFWwindow *mWindow;
    GLFWInput() {}
    GLFWInput(GLFWInput const&);
    void operator=(GLFWInput const&);
    
public:
    
	static bool keys[1024];


    Signal<int, int, int, int> keyEvent;
    Signal<double, double> mouseMoveEvent;
    Signal<int, int, int> mouseButtonEvent;
    
    static GLFWInput& getInstance()
    {
        static GLFWInput instance;
        return instance;
    }
    
    void Init(GLFWwindow &window);
    
};


#endif /* GLFWInput_hpp */
