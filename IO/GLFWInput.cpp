//
//  GLFWInputDevice.cpp
//  render_design
//
//  Created by Chebonenko Vitali on 4/7/17.
//  Copyright © 2017 Chebonenko Vitali. All rights reserved.
//

#include "GLFWInput.hpp"
#include "GLToolkit.hpp"

bool GLFWInput::keys[1024];


void GLFWInput::Init(GLFWwindow &window)
{
    mWindow = &window;
    glfwSetKeyCallback(mWindow, [](GLFWwindow* mWindow, int key, int scancode, int action, int mods)
                       {
							if (action == GLFW_PRESS)
								GLFWInput::keys[key] = true;
							else if (action == GLFW_RELEASE)
								GLFWInput::keys[key] = false;

                           GLFWInput::getInstance().keyEvent.emit(key, scancode, action, mods);
                       });
    glfwSetCursorPosCallback(mWindow, [](GLFWwindow* mWindow, double xpos, double ypos)
                             {
                                 GLFWInput::getInstance().mouseMoveEvent.emit(xpos, ypos);
                             });
    glfwSetMouseButtonCallback(mWindow, [](GLFWwindow* mWindow, int button, int action, int mods)
                               {
                                   GLFWInput::getInstance().mouseButtonEvent.emit(button, action, mods);
                               });
}
