//
//  Subject.hpp
//  render_design
//
//  Created by Chebonenko Vitali on 4/19/17.
//  Copyright © 2017 Chebonenko Vitali. All rights reserved.
//

#ifndef Subject_hpp
#define Subject_hpp

#include <stdio.h>
#include <vector>
#include <unordered_map>
#include "Observer.hpp"
#include <functional>
#include "Signal.hpp"

//http://stackoverflow.com/questions/14677997/stdfunction-vs-template
//http://stackoverflow.com/questions/25848690/should-i-use-stdfunction-or-a-function-pointer-in-c
// http://stackoverflow.com/questions/22965647/how-to-create-a-variadic-template-function-with-stdfunction-as-a-function-pa
//http://stackoverflow.com/questions/9568150/what-is-a-c-delegate
//http://blog.coldflake.com/posts/C++-delegates-on-steroids/
//https://habrahabr.ru/post/78299/
//http://www.codeproject.com/Articles/7150/Member-Function-Pointers-and-the-Fastest-Possible



class Subject
{
    
    std::vector<Observer*> mObservers;
    std::vector<std::function<void()>> functors;
    
public:
    
    template<typename... Args>
    void notify(Args&&... args) 
    {
        for (const auto obs : mObservers)
        {
            //obs->receive(std::forward<Args>(args)...);
        }
    }
    
    template<class T, typename... Args>
    void subscribe(T* obs, void(T::*method)(Args...), Args... args)
    {
        mObservers.push_back(obs);
    }
    
    void addObserver(Observer* observer)
    {
        mObservers.push_back(observer);
    }
    void removeObserver(Observer* observer)
    {
        //mObservers.erase(std::remove(mObservers.begin(), mObservers.end(), observer), mObservers.end());
    }
};

#endif /* Subject_hpp */
