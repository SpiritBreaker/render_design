//
//  Observer.hpp
//  render_design
//
//  Created by Chebonenko Vitali on 4/19/17.
//  Copyright © 2017 Chebonenko Vitali. All rights reserved.
//
// https://jguegant.github.io/blogs/tech/thread-safe-multi-type-map.html
// https://github.com/fnz/ObserverManager/blob/master/ObserverManager


#ifndef Observer_hpp
#define Observer_hpp

#include <stdio.h>
#include <iostream>

class Observer
{
    
public:
    
    template <typename... Args>
    void receive(Args... args)
    {
//        int dummy[sizeof...(Args)] = {
//            (std::cout << args << std::endl, 0)...
//        };
    }
    
    
};


#endif /* Observer_hpp */
