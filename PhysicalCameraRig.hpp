//
//  CameraRig.hpp
//  render_design
//
//  Created by Chebonenko Vitali on 3/22/17.
//  Copyright © 2017 Chebonenko Vitali. All rights reserved.
//

#ifndef PhysicalCameraRig_hpp
#define PhysicalCameraRig_hpp

#include <stdio.h>
#include "glm/glm.hpp"

struct Camera;
struct Transform;
class Renderer;

class PhysicalCameraRig
{
    
private:

	Renderer* m_renderer;
    Camera* m_camera;

	float MOVEMENT_SPEED = 0.1f;

	glm::vec3 camera_front = glm::vec3(0.0f, 0.0f, -1.0f);



	float lastX = 640 / 2.0,
		lastY = 480 / 2.0,
		yaw = -90.0f,
		pitch = 0.0f;

	bool firstMouse;

	float magnitude = 0.0f;
	glm::vec3 velocity_vector;
	float camera_speed = 0.0f;
	float max_speed = 0.5f;
	float accel_rate = 0.1f;


public:
    
	PhysicalCameraRig(Camera &camera, Renderer &renderer);
    void setUpCamera(Camera &camera);

    void rotate();
    void pan();
    void zoom();
    
    void move_forward();
    void move_backward();
    void strafe_left();
    void strafe_right();

    void set_default_position();
    
    void mouseMoveEvent(double, double);
    void mouseButtonEvent(int, int, int);
    void keyEvent(int, int, int, int);

	void do_movements();

public:
    
    
};

#endif /* PhysicalCameraRig_hpp */
