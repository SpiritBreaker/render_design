//
//  scene.cpp
//  render_design
//
//  Created by Chebonenko Vitali on 3/10/17.
//  Copyright © 2017 Chebonenko Vitali. All rights reserved.
//

// OBJ file specification
//http://www.martinreddy.net/gfx/3d/OBJ.spec 


#define TINYOBJLOADER_IMPLEMENTATION
#define	STB_IMAGE_IMPLEMENTATION


#include "scene.hpp"
#include "tiny_obj_loader.h"
#include <vector>
#include <cstdio>
#include "shaderset.hpp"
#include "log.h"
#include "GL.hpp"
#include "GLToolkit.hpp"
#include "stb/stb_image.h"

#include <fstream>

uint32_t createMesh(Scene& scene,
                const std::string& name,
                const std::vector<float>& positions,
                const std::vector<float>& texcoords,
                const std::vector<float>& normals,
                const std::vector<unsigned int>& indices)
{
	GLuint positionVBO = 0;
	GLuint	texcoordVBO = 0;
	GLuint	normalVBO = 0;
	GLuint	indiciesEBO = 0;
	GLuint	meshVAO = 0;
    
    if(!positions.empty())
    {
        glGenBuffers(1, &positionVBO);
        glBindBuffer(GL_ARRAY_BUFFER, positionVBO);
        glBufferData(GL_ARRAY_BUFFER,
                     positions.size() * sizeof(float),
                     positions.data(), GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }
    
    if(!texcoords.empty())
    {
        glGenBuffers(1, &texcoordVBO);
        glBindBuffer(GL_ARRAY_BUFFER, texcoordVBO);
        glBufferData(GL_ARRAY_BUFFER,
                     texcoords.size() * sizeof(float),
                     texcoords.data(), GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }
    
    if(!normals.empty())
    {
        glGenBuffers(1, &normalVBO);
        glBindBuffer(GL_ARRAY_BUFFER, normalVBO);
        glBufferData(GL_ARRAY_BUFFER,
                     normals.size()*sizeof(float),
                     normals.data(), GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }
    
    if(!indices.empty())
    {
        glGenBuffers(1, &indiciesEBO);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indiciesEBO);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                     indices.size() * sizeof(unsigned int),
                     indices.data(), GL_STATIC_DRAW);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    }
    
    glGenVertexArrays(1, &meshVAO);
    
    if (positionVBO != 0)
    {
        glBindVertexArray(meshVAO);
        glBindBuffer(GL_ARRAY_BUFFER, positionVBO);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(float) *3, 0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glEnableVertexAttribArray(0);
        glBindVertexArray(0);
    }
    
    if (texcoordVBO != 0)
    {
        glBindVertexArray(meshVAO);
        glBindBuffer(GL_ARRAY_BUFFER, texcoordVBO);
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(float) *2, 0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glEnableVertexAttribArray(1);
        glBindVertexArray(0);
    }
    
    if (normalVBO != 0)
    {
        glBindVertexArray(meshVAO);
        glBindBuffer(GL_ARRAY_BUFFER, normalVBO);
        glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(float) *3, 0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glEnableVertexAttribArray(2);
        glBindVertexArray(0);
    }
    
    if (indiciesEBO != 0)
    {
        glBindVertexArray(meshVAO);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indiciesEBO);
        glBindVertexArray(0);
    }
    
    glBindVertexArray(meshVAO);
    glBindVertexArray(0);
    


    //Create mesh
    Mesh *newMesh = new Mesh();
    newMesh->Name = name;
    newMesh->IndexCount = (GLuint)indices.size();
    newMesh->VertexCount = (GLuint)positions.size();
    
    newMesh->MeshVAO =  meshVAO;
    newMesh->PositionBO =  positionVBO;
    newMesh->TexCoordBO = texcoordVBO;
    newMesh->NormalBO = normalVBO;
    newMesh->IndexBO = indiciesEBO;
    
    //Add mesh to scene graph
    uint32_t meshId = scene.Meshes.add(newMesh);
    scene.loadedMeshIDs.push_back(meshId);
    
    //Create transform and add to the graph
    //Change this block to a function called AddInstance()
    Transform *newTransform = new Transform();
    scene.Transforms.add(newTransform);

	return meshId;
}

void loadMeshes(Scene& scene,
                const std::string& filename)
{
    std::vector<tinyobj::shape_t> shapes;
    std::vector<tinyobj::material_t> materials;
	std::string err;
	
	// assume mtl is in the same folder as the obj
	std::string mtl_basepath = filename;
	size_t last_slash = mtl_basepath.find_last_of("/");
	if (last_slash == std::string::npos)
		mtl_basepath = "./";
	else
		mtl_basepath = mtl_basepath.substr(0, last_slash + 1);
    
	if (!tinyobj::LoadObj(shapes, materials, err,
		filename.c_str(), mtl_basepath.c_str(),
		tinyobj::triangulation | tinyobj::calculate_normals))
    {
        fprintf(stderr, "Failed to load: %s\n", err.c_str());
    }

	//std::vector<uint32_t> materialIds = loadMaterials(scene, mtl_basepath);

    GLuint positionVBO = 0;
    GLuint texcoordVBO = 0;
    GLuint normalVBO = 0;
    GLuint indiciesEBO = 0;
    GLuint meshVAO = 0;

    for (const auto& shape: shapes)
    {
        positionVBO = 0;
        texcoordVBO = 0;
        normalVBO = 0;
        indiciesEBO = 0;
        meshVAO = 0;
        
        if(!shape.mesh.positions.empty())
        {
            glGenBuffers(1, &positionVBO);
            glBindBuffer(GL_ARRAY_BUFFER, positionVBO);
            glBufferData(GL_ARRAY_BUFFER,
                         shape.mesh.positions.size() * sizeof(float),
                         shape.mesh.positions.data(), GL_STATIC_DRAW);
            glBindBuffer(GL_ARRAY_BUFFER, 0);
        }
        
        if(!shape.mesh.texcoords.empty())
        {
            glGenBuffers(1, &texcoordVBO);
            glBindBuffer(GL_ARRAY_BUFFER, texcoordVBO);
            glBufferData(GL_ARRAY_BUFFER,
                        shape.mesh.texcoords.size() * sizeof(float),
                        shape.mesh.texcoords.data(), GL_STATIC_DRAW);
            glBindBuffer(GL_ARRAY_BUFFER, 0);
        }
        
        if(!shape.mesh.normals.empty())
        {
            glGenBuffers(1, &normalVBO);
            glBindBuffer(GL_ARRAY_BUFFER, normalVBO);
            glBufferData(GL_ARRAY_BUFFER,
                         shape.mesh.normals.size()*sizeof(float),
                         shape.mesh.normals.data(), GL_STATIC_DRAW);
            glBindBuffer(GL_ARRAY_BUFFER, 0);
        }
        
        if(!shape.mesh.indices.empty())
        {
            glGenBuffers(1, &indiciesEBO);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indiciesEBO);
            glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                         shape.mesh.indices.size() * sizeof(unsigned int),
                         shape.mesh.indices.data(), GL_STATIC_DRAW);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        }
        
        glGenVertexArrays(1, &meshVAO);
        
        if (positionVBO != 0)
        {
            glBindVertexArray(meshVAO);
            glBindBuffer(GL_ARRAY_BUFFER, positionVBO);
            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(float) *3, 0);
            glBindBuffer(GL_ARRAY_BUFFER, 0);
            glEnableVertexAttribArray(0);
            glBindVertexArray(0);
        }
        
        if (texcoordVBO != 0)
        {
            glBindVertexArray(meshVAO);
            glBindBuffer(GL_ARRAY_BUFFER, texcoordVBO);
            glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(float) *2, 0);
            glBindBuffer(GL_ARRAY_BUFFER, 0);
            glEnableVertexAttribArray(1);
            glBindVertexArray(0);
        }
        
        if (normalVBO != 0)
        {
            glBindVertexArray(meshVAO);
            glBindBuffer(GL_ARRAY_BUFFER, normalVBO);
            glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(float) *3, 0);
            glBindBuffer(GL_ARRAY_BUFFER, 0);
            glEnableVertexAttribArray(2);
            glBindVertexArray(0);
        }
        
        if (indiciesEBO != 0)
        {
            glBindVertexArray(meshVAO);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indiciesEBO);
            glBindVertexArray(0);
        }
        
        glBindVertexArray(meshVAO);
        glBindVertexArray(0);
        
        //Create mesh
        Mesh *newMesh = new Mesh();
        newMesh->Name = shape.name;
        newMesh->IndexCount = (GLuint)shape.mesh.indices.size();
        newMesh->VertexCount = (GLuint)shape.mesh.positions.size();
        
        newMesh->MeshVAO =  meshVAO;
        newMesh->PositionBO =  positionVBO;
        newMesh->TexCoordBO = texcoordVBO;
        newMesh->NormalBO = normalVBO;
        newMesh->IndexBO = indiciesEBO;
        newMesh->MaterialID = scene.Materials.query(&Material::Name,  materials.at(shape.mesh.material_ids.at(0)).name);

        //Add mesh to scene graph
        uint32_t meshId = scene.Meshes.add(newMesh);
        scene.loadedMeshIDs.push_back(meshId);
        
        //Create transform and add to the graph
        //Change this block to a function called AddInstance()
        Transform *newTransform = new Transform();
        scene.Transforms.add(newTransform);
    }
}

uint32_t loadShaderProgram(Scene& scene,
                       const std::string& name,
                       const GLuint& vertexShader,
                       const GLuint& pixelShader)
{
    GLuint program = createShaderProgram(vertexShader, pixelShader);
    ShaderProgram *newShaderProgram = new ShaderProgram();
    newShaderProgram->Name = name;
    newShaderProgram->ProgramID = program;
    
    uint32_t programId = scene.ShaderPrograms.add(newShaderProgram);
	scene.loadedShaderProgramIDs.push_back(programId);

	return programId;
}


uint32_t loadShader(Scene& scene,
                const std::string& fileName,
                const std::string& name,
                const GLenum& type)
{
    GLuint shader = createShader(type, fileName.c_str());
    assert(shader != 0);
    
    Shader* newShader = new Shader();
    newShader->Name = name;
    newShader->ShaderBO = shader;
    newShader->Type = type;

    uint32_t shaderId = scene.Shaders.add(newShader);
    scene.loadedShaderIDs.push_back(shaderId);
	return shaderId;
}


uint32_t loadTexture(Scene& scene,
	const std::string& filename)
{

	FILE_LOG(logINFO) << "Load texture: " << filename;

	int width, height, comp;

	stbi_set_flip_vertically_on_load(1);

	unsigned char *data = stbi_load(
		filename.c_str(),
		&width, 
		&height, 
		&comp, 
		STBI_rgb);

	stbi_set_flip_vertically_on_load(0);

	if (!data)
	{
		FILE_LOG(logERROR) << "failed to load texture map: " << filename;
		return 0;
	}
	
	GLuint textureBO;
	glGenTextures(1, &textureBO);
	if (textureBO == 0)
	{
		FILE_LOG(logERROR) << "failed to load texture map: " << filename;
		return 0;
	}

	//float maxAnisotropy;
    //glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &maxAnisotropy);

	glBindTexture(GL_TEXTURE_2D, textureBO);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	//glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, GL_RGB, GL_UNSIGNED_BYTE, data);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	//glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, maxAnisotropy);
	//glGenerateMipmap(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, 0);

	TextureMap* newTexture = new TextureMap();
	newTexture->Name = filename;
	newTexture->TextureMapBO = textureBO;

	uint32_t textureMapId = scene.TextureMaps.add(newTexture);
	scene.loadedTextureIDs.push_back(textureMapId);

	FILE_LOG(logINFO) << "Texture has loaded ID: " << textureMapId;

	stbi_image_free(data);
	return textureMapId;
}


std::vector<uint32_t> loadMaterials(
	Scene& scene,
	const std::string& filename)
{
	std::vector<uint32_t> loadedIDs;

	std::map<std::string, int> material_map;
	std::vector<tinyobj::material_t> materials;

	std::filebuf fb;
	if (fb.open(filename.c_str(), std::ios::in))
	{
		std::istream is(&fb);
		tinyobj::LoadMtl(material_map, materials, is);
		fb.close();
	}

	//assume that textures are in the same folder where mtl file
	std::string path_to_mtl = filename;
	path_to_mtl.erase(path_to_mtl.find_last_of("/"), path_to_mtl.length());

	for (auto material : materials)
	{
        if (scene.Materials.query(&Material::Name, material.name))
        {
            FILE_LOG(logINFO) << material.name  << " already exist. Skipped";
            continue;
        }
        
		Material* newMaterial = new Material();
		newMaterial->Name = material.name;
		newMaterial->TextureMapID = loadTexture(scene, path_to_mtl + "/" + material.diffuse_texname);
		//newMaterial->NormalMapID = loadTexture(scene, material.bump_texname);
		uint32_t materialID = scene.Materials.add(newMaterial);
		scene.loadedMaterialIDs.push_back(materialID);
		loadedIDs.push_back(materialID);
	}

	return loadedIDs;
}
