//
//  GLRenderer.cpp
//  render_design
//
//  Created by Chebonenko Vitali on 3/14/17.
//  Copyright © 2017 Chebonenko Vitali. All rights reserved.
//

#include "GL.hpp"
#include "GLToolkit.hpp"
#include "GLFWRenderer.hpp"
#include "GLRender.hpp"
#include "systemDefs.hpp"
#include "scene.hpp"
#include <functional>
#include <glm/gtc/matrix_transform.hpp>
#include "log.h"


float GLFWRenderer::deltaTime = 0.f;
int GLFWRenderer::frame_buffer_width = WIDTH;
int GLFWRenderer::frame_buffer_height = HEIGHT;

GLFWwindow &GLFWRenderer::window()
{
    return *mWindow;
}

GLRender &GLFWRenderer::get_GLRender()
{
	return *mGLRender;
}

void GLFWRenderer::Init()
{
    

    //glfwSetErrorCallback(error_callback);
    if(!glfwInit())
    {
        fprintf(stderr, "ERROR: could not open window with GLFW3\n");
    }
    

    //init opengl 4

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
    
    mWindow = glfwCreateWindow(WIDTH, HEIGHT, TITLE, NULL, NULL);
    if (!mWindow)
    {
        glfwTerminate();
        exit(EXIT_FAILURE);
    }
    glfwMakeContextCurrent(mWindow);
	glfwSetInputMode(mWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    glfwGetFramebufferSize(mWindow, &frame_buffer_width, &frame_buffer_height);
    glfwSetWindowAspectRatio(mWindow, 16, 9);
    
#ifdef _WIN32
	glewInit();
#endif

    if (GRAPHICS_INFO)
    {
        const GLubyte* graphics = glGetString(GL_RENDERER);
        const GLubyte* vendor = glGetString(GL_VENDOR);
        const GLubyte* version = glGetString(GL_VERSION);
        const GLubyte* shader = glGetString(GL_SHADING_LANGUAGE_VERSION);
        
        GLint major, minor;
        glGetIntegerv(GL_MAJOR_VERSION, &major);
        glGetIntegerv(GL_MINOR_VERSION, &minor);
        
        GLint nExtensions;
        glGetIntegerv(GL_NUM_EXTENSIONS, &nExtensions);
        
        printf("Renderer                                : %s\n",     graphics);
        printf("Vendor                                  : %s\n",     vendor);
        printf("OpenGL version supported(string)        : %s\n",     version);
		printf("OpenGL version supported(integer)       : %d, %d\n", major, minor);
        printf("OpenGL shader version supported         : %s\n",     shader);
        
        //for (int i = 0; i < nExtensions; i++)
        //    printf("%s\n", glGetStringi(GL_EXTENSIONS, i));
    }
    
}

void GLFWRenderer::Resize(int width, int height)
{
    glfwGetWindowSize(mWindow, &width, &height);
}

void GLFWRenderer::change_mode()
{
	if (glfwGetInputMode(mWindow, GLFW_CURSOR) == GLFW_CURSOR_DISABLED)
		glfwSetInputMode(mWindow, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	else
		glfwSetInputMode(mWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
}

void GLFWRenderer::game_loop(std::vector<std::function<void()>> &functions_queue)
{
	while (!glfwWindowShouldClose(mWindow))
	{
		GLfloat currentFrame = (float)glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;
		
		for (auto function : functions_queue)
			function();

		glfwSwapBuffers(mWindow);
		glfwPollEvents();
	}

	glfwDestroyWindow(mWindow);
	glfwTerminate();
}

int GLFWRenderer::width()
{
	int width;
	int height;
	glfwGetWindowSize(mWindow, &width, &height);
	return width;
}

int GLFWRenderer::height()
{
	int width;
	int height;
	glfwGetWindowSize(mWindow, &width, &height);
	return height;
}
