//
//  CameraRig.cpp
//  render_design
//
//  Created by Chebonenko Vitali on 3/22/17.
//  Copyright © 2017 Chebonenko Vitali. All rights reserved.
//
// https://learnopengl.com/#!Getting-started/Camera


#include "CameraRig.hpp"
#include "Renderer.hpp"
#include "scene.hpp"
#include "log.h"
#include "GLFWAdapter.hpp"
#include <glm/gtc/matrix_transform.hpp>

#include "GLFWInput.hpp"
#include "GLFWRenderer.hpp"
#include "glm/gtx/string_cast.hpp"

CameraRig::CameraRig(Camera &camera, Renderer &renderer) : m_camera(&camera), m_renderer(&renderer)
{

};

void CameraRig::setUpCamera(Camera &camera)
{
    m_camera = &camera;
}

void CameraRig::move_forward()
{
	m_camera->position += camera_speed * m_camera->target;
}

void CameraRig::move_backward()
{
	m_camera->position -= camera_speed * m_camera->target;
}

void CameraRig::strafe_right()
{
	m_camera->position += glm::normalize(glm::cross(m_camera->target, m_camera->up)) * camera_speed;
}

void CameraRig::strafe_left()
{
	m_camera->position -= glm::normalize(glm::cross(m_camera->target, m_camera->up)) * camera_speed;
}

void CameraRig::move_up()
{
	m_camera->position += camera_speed * m_camera->up;
}

void CameraRig::move_down()
{
	m_camera->position += -camera_speed * m_camera->up;
}

void CameraRig::set_default_position()
{
    std::cout << "set default position" << std::endl;
    m_camera->position = glm::vec3(0.f, 0.f, 3.f);
    m_camera->target = glm::vec3(0.f, 0.f, -1.f);
    m_camera->up = glm::vec3(0.f, 1.f, 0.f);
}

void CameraRig::mouseMoveEvent(double xpos, double ypos)
{

	if (firstMouse) // this bool variable is initially set to true
	{

		lastX = xpos;
		lastY = ypos;
		firstMouse = false;
	}

	float xoffset = xpos - lastX;
	float yoffset = lastY - ypos; // Reversed since y-coordinates range from bottom to top
	lastX = xpos;
	lastY = ypos;

	float sensitivity = 0.05f;
	xoffset *= sensitivity;
	yoffset *= sensitivity;

	yaw += xoffset;
	pitch += yoffset;

	if (pitch > 89.0f)
		pitch = 89.0f;
	if (pitch < -89.0f)
		pitch = -89.0f;


	glm::vec3 front;
	front.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
	front.y = sin(glm::radians(pitch));
	front.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
	m_camera->target = glm::normalize(front);

}

void CameraRig::mouseButtonEvent(int button, int action, int mods)
{
	
}

void CameraRig::keyEvent(int key, int scancode, int action, int mods)
{
    switch (key)
    {
        case KEY_W:
            this->move_forward();
            break;
            
        case KEY_S:
            this->move_backward();
            break;
            
        case KEY_A:
            this->strafe_left();
            break;
            
        case KEY_D:
            this->strafe_right();
            break;
            
        case KEY_R:
            this->move_up();
            break;
            
        case KEY_F:
            this->move_down();
            break;
    }
}


void CameraRig::do_movements()
{
	glm::vec3 start_position = m_camera->position;
	camera_speed = MOVEMENT_SPEED * GLFWRenderer::deltaTime;

	if (GLFWInput::keys[KEY_W])
		this->move_forward();

	if (GLFWInput::keys [KEY_S])
		this->move_backward();
		
	if (GLFWInput::keys [KEY_A])
		this->strafe_left();

	if (GLFWInput::keys [KEY_D])
		this->strafe_right();
		
	if (GLFWInput::keys [KEY_R])
		this->move_up();
		
	if (GLFWInput::keys [KEY_F])
        this->set_default_position();
    

	
}

