//
//  CameraRig.hpp
//  render_design
//
//  Created by Chebonenko Vitali on 3/22/17.
//  Copyright © 2017 Chebonenko Vitali. All rights reserved.
//

#ifndef CameraRig_hpp
#define CameraRig_hpp

#include <stdio.h>
#include "glm/glm.hpp"

struct Camera;
struct Transform;
class Renderer;

class CameraRig
{
    
private:

	Renderer* m_renderer;
    Camera* m_camera;

	float MOVEMENT_SPEED = 5.5f;

	glm::vec3 camera_front = glm::vec3(0.0f, 0.0f, -1.0f);

	float lastX = 640 / 2.0,
		lastY = 480 / 2.0,
		yaw = -90.0f,
		pitch = 0.0f;

	bool firstMouse;

	glm::vec3 movement_vector;
	float camera_speed = 0.0f;
	float max_speed = 2.f;
	float accel_rate = 0.1f;

	float forward_speed = 0.f;
	float backward_speed = 0.f;
	float strafe_left_speed = 0.f;
	float strafe_right_speed = 0.f;

public:
    
	CameraRig(Camera &camera, Renderer &renderer);
    void setUpCamera(Camera &camera);

    void rotate();
    void pan();
    void zoom();
    
    void move_forward();
    void move_backward();
    void strafe_left();
    void strafe_right();
    void move_up();
    void move_down();
	void move();
    
    void set_default_position();
    
    void mouseMoveEvent(double, double);
    void mouseButtonEvent(int, int, int);
    void keyEvent(int, int, int, int);

	void do_movements();

public:
    
    
};

#endif /* CameraRig_hpp */
