#!lua

-- A solution contains projects, and defines the available configurations
solution "render_design"
    configurations { "Debug", "Release" }
    platforms { "Universal", "x64" } --universal - Mac OS X, both 32 and 64 bit
    location ("./projects")
    if os.get() == "windows" then
    	code_base_path = "C:/Users/v_chebonenko/Google Drive/codebase/development"

    else
        if os.get() == "macosx" then
	        qt_path = "" -- put path to qt here for macosx
		    end
	    end
		
    cwd = os.getcwd() --current working directory
   
   -- A project defines one build target
    project "render_design"
        kind "ConsoleApp"
        language "C++"
        files { "**.h", "**.hpp", "**.cpp" ,"**.glsl", "**.cu", }
		
		configuration {"Release", "x64" }
            defines { "NDEBUG" }
            flags { "Optimize" }
		    targetdir "./bin/release"
		    links {  "glfw3", "opengl32", "glew32" }

        configuration {"Debug", "x64" }
            defines { "DEBUG" }
            flags { "Symbols" }
		    targetdir "./bin/debug/"
		    links {  "glfw3", "opengl32", "glew32" }
					
        configuration{}		
		
	    libdirs { code_base_path .. "/library"}
		includedirs {
		             cwd, 
		             cwd.."/**", 
		             code_base_path .. "/include", 
					}
		location ("./projects")

		