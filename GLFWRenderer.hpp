//
//  GLRenderer.hpp
//  render_design
//
//  Created by Chebonenko Vitali on 3/14/17.
//  Copyright © 2017 Chebonenko Vitali. All rights reserved.
//

#ifndef GLFWRenderer_hpp
#define GLFWRenderer_hpp

#include <stdio.h>
#include "Renderer.hpp"
#include <functional>
#include <vector>

struct GLFWwindow;
class GLRender;

class GLFWRenderer : public Renderer
{  
    GLFWwindow *mWindow;
    GLRender *mGLRender;
    
public:

	static float deltaTime;
    static int frame_buffer_width;
    static int frame_buffer_height;
    
	float lastFrame = 0.0f;

    GLFWRenderer(GLRender &render) : mGLRender(&render)
    {
    }
    
    void Init();
    void Resize(int width, int height);
	void change_mode();
	void game_loop(std::vector<std::function<void()>> &functions_queue);
	int width();
	int height();
    
    GLFWwindow &window();
	GLRender &get_GLRender();
    
};

#endif /* GLFWRenderer_hpp */
