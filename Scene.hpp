//
//  scene.hpp
//  render_design
//
//  Created by Chebonenko Vitali on 3/10/17.
//  Copyright © 2017 Chebonenko Vitali. All rights reserved.
//

#ifndef scene_hpp
#define scene_hpp
#include <stdio.h>
#include <stdint.h>
#include <vector>
#include <string>
#include "lookup_table.hpp"
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

typedef unsigned int GLuint;
typedef unsigned int GLenum;


struct Mesh
{
	std::string Name;
	GLuint MeshVAO;
	GLuint PositionBO;
	GLuint TexCoordBO;
	GLuint NormalBO;
	GLuint IndexBO;

	GLuint IndexCount;
	GLuint VertexCount;

	//std::vector<uint32_t> MaterialIDs;
	uint32_t MaterialID;
};

struct TextureMap
{
	std::string Name;
	GLuint TextureMapBO;
};

struct Material
{
	std::string Name;
	float Ambient[3];
	float Diffuse[3];
	float Scpecular[3];

	float Shiness;
	GLuint TextureMapID;
	//uint32_t NormalMapID;
};

struct Shader
{
	std::string Name;
	GLuint ShaderBO;
	GLenum Type;
};

struct ShaderProgram
{
	std::string Name;
	GLuint ProgramID;
};

struct Transform
{
	glm::vec3 Scale;
	glm::vec3 RotationOrigin;
	glm::quat Rotation;
	glm::vec3 Translation;
};

struct Instance
{
	uint32_t MeshID;
	uint32_t TransformID;
};

struct Camera
{
	std::string name;
	glm::vec3 position;
	glm::vec3 target;
	glm::vec3 up;

	//Projection
	float FovY;
	float Aspect;
	float ZNear;
	float ZFar;
};

class Scene
{
public:

	std::vector<uint32_t> loadedMeshIDs;
	std::vector<uint32_t> loadedMaterialIDs;
	std::vector<uint32_t> loadedShaderIDs;
	std::vector<uint32_t> loadedCamerasIDs;
	std::vector<uint32_t> loadedShaderProgramIDs;
	std::vector<uint32_t> loadedTextureIDs;

	lookup_table<TextureMap> TextureMaps;
	lookup_table<Material> Materials;
	lookup_table<Mesh> Meshes;
	lookup_table<Transform> Transforms;
	lookup_table<Instance> Instances;
	lookup_table<Camera> Cameras;
	lookup_table<Shader> Shaders;
	lookup_table<ShaderProgram> ShaderPrograms;

	uint32_t MainCameraID;
};


void loadMeshes(Scene& scene,
                const std::string& filename);

uint32_t loadShader(Scene& scene,
                const std::string& filename,
                const std::string& name,
                const GLenum& type);

uint32_t loadShaderProgram(Scene& scene,
                       const std::string& name,
                       const GLuint& vertexShader,
                       const GLuint& pixelShader);

uint32_t createMesh(Scene& scene,
                const std::string& name,
                const std::vector<float>& positions,
                const std::vector<float>& texcoords,
                const std::vector<float>& normals,
                const std::vector<unsigned int>& indices);

uint32_t loadTexture(Scene& scene,
				const std::string& filename);

std::vector<uint32_t> loadMaterials(
	Scene& scene,
	const std::string& filename);


#endif /* scene_hpp */
