//
//  SystemDefs.hpp
//  render_design
//
//  Created by Chebonenko Vitali on 4/3/17.
//  Copyright © 2017 Chebonenko Vitali. All rights reserved.
//

#ifndef SystemDefs_hpp
#define SystemDefs_hpp
#include <stdio.h>


//ger real retina display size

#define WIDTH 1000
#define HEIGHT 800
#define FRAME_BUFFER_WIDTH WIDTH
#define FRAME_BUFFER_HEIGHT HEIGHT
#define GRAPHICS_INFO true
#define TITLE "OpenGL Render"

#ifdef _WIN64
//define something for Windows (64-bit)
#elif _WIN32
//define something for Windows (32-bit)
#elif __APPLE__
#include "TargetConditionals.h"
#if TARGET_OS_IPHONE && TARGET_IPHONE_SIMULATOR
// define something for simulator
#elif TARGET_OS_IPHONE
// define something for iphone
#else
#define TARGET_OS_OSX 1
// define something for OSX
#endif
#elif __linux
// linux
#elif __unix // all unices not caught above
// Unix
#elif __posix
// POSIX
#endif


#endif /* SystemDefs_hpp */
