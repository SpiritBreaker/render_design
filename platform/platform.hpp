
#ifdef __APPLE__

#include <mach-o/dyld.h>
#include <iostream>

namespace platform {
    std::string exec_dir()
    {
        char result[1024];
        uint32_t size = sizeof(result);
        _NSGetExecutablePath(result, &size);
        std::string full_path = std::string(result);
        std::string base = full_path.substr(0, full_path.find_last_of("/"));
        return base;
    }
}

#endif

#ifdef _WIN32
#include <windows.h>
#include <iostream>

namespace platform {

	std::string exec_dir()
	{
		char result[MAX_PATH];
		std::string full_path = std::string(result, GetModuleFileName(NULL, result, MAX_PATH));
		std::string base = full_path.substr(0, full_path.find_last_of("\\"));
		return base;
	}
}

#endif

