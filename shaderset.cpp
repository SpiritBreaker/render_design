//
//  shaderset.cpp
//  render_design
//
//  Created by Chebonenko Vitali on 4/3/17.
//  Copyright © 2017 Chebonenko Vitali. All rights reserved.
//

#include "shaderset.hpp"
#include <fstream>
#include <assert.h>
#include "log.h"
#include <GL.hpp>
#include <GLToolkit.hpp>
//
//  type of shaders
//  GL_VERTEX_SHADER
//  GL_FRAGMENT_SHADER
//  GL_TESS_CONTROL_SHADER
//  GL_TESS_EVALUATION_SHADER
//  GL_GEOMETRY_SHADER
//

bool checkStatus(GLuint objectID,
	PFNGLGETSHADERIVPROC objectPropertyGetterFunc,
	PFNGLGETSHADERINFOLOGPROC getInfoLogFunc,
	GLenum statusType)

{
	GLint status;
	objectPropertyGetterFunc(objectID, statusType, &status);
	if (status != GL_TRUE)
	{
		FILE_LOG(logERROR) << "Shader compilation failed";
		GLint infoLogLength;
		objectPropertyGetterFunc(objectID, GL_INFO_LOG_LENGTH, &infoLogLength);
		GLchar* buffer = new GLchar[infoLogLength];

		GLsizei bufferSize;
		getInfoLogFunc(objectID, infoLogLength, &bufferSize, buffer);
		std::cout << buffer << std::endl;

		delete[] buffer;
		return false;
	}

	return true;
}


GLuint createShader(GLenum type, const char* fileName){
    
    FILE_LOG(logINFO) << "Create shader: " << fileName;
    
    GLuint shader = glCreateShader(type);
    const GLchar* adapter[1];
    std::string temp = readShaderCode(fileName);
    adapter[0] = temp.c_str();
    
    glShaderSource(shader, 1, adapter, 0);
    glCompileShader(shader);
    
    if (!checkShaderStatus(shader))
    {
        FILE_LOG(logERROR) << "checkShaderStatus failed " << fileName;
        return 0;
    }
    
    FILE_LOG(logINFO) << "Shader is created: ID: " << shader;

    return shader;
};

GLuint createShaderProgram(const GLuint &vertexShader, const GLuint &pixelShader)
{
    FILE_LOG(logINFO) << "BEGIN: Create shader programm with VS: " << vertexShader << " PS: " << pixelShader;
    GLuint programID = glCreateProgram();
    glAttachShader(programID, vertexShader);
    glAttachShader(programID, pixelShader);
    glLinkProgram(programID);
    if (!checkProgramStatus(programID))
    {
        FILE_LOG(logERROR) << "checkProgramStatus failed ";
        return 0;
    }
    FILE_LOG(logINFO) << "Shader program has been created ID: " << programID;
    FILE_LOG(logINFO) << "END: Create shader programm with VS: " << vertexShader << " PS: " << pixelShader;
    
    return programID;
}



std::string readShaderCode(const char* fileName)
{
    std::ifstream meInput(fileName);
    if (!meInput.good())
    {
        std::cout << "File failed to read " << fileName << std::endl;
        exit(1);
    }
    return std::string(
                       std::istreambuf_iterator<char>(meInput),
                       std::istreambuf_iterator<char>());
}




bool checkShaderStatus(GLuint shaderID)
{
    return checkStatus(shaderID, glGetShaderiv, glGetShaderInfoLog, GL_COMPILE_STATUS);
}

bool checkProgramStatus(GLuint programID)
{
    return checkStatus(programID, glGetProgramiv, glGetProgramInfoLog, GL_LINK_STATUS);
}
