
//
//  main.cpp
//  render_design
//
//  Created by Chebonenko Vitali on 3/1/17.
//  Copyright © 2017 Chebonenko Vitali. All rights reserved.
//
//  Most of the render design features have been taken from here
//  https://nlguillemot.wordpress.com/2016/11/18/opengl-renderer-design/
//  https://github.com/nlguillemot/dof/tree/master/viewer
//  http://simmesimme.github.io/tutorials/2015/09/20/signal-slot


#include "GL.hpp"
#include <iostream>
#include <fstream>
#include <memory>
#include "log.h"
#include "scene.hpp"
#include "Renderer.hpp"
#include "GLFWRenderer.hpp"
#include "GLRender.hpp"
#include "GLFWInput.hpp"
#include "systemDefs.hpp"
#include "PhysicalCameraRig.hpp"
#include "platform.hpp"


using namespace std;

int main(int argc, const char * argv[]) {
    
    //liba *test = new liba();
    //test->foo();
    
    Scene *scene = new Scene();
    GLRender *simpleRender = new GLRender(*scene);
    GLFWRenderer* renderer = new GLFWRenderer(*simpleRender);
    renderer->Init();

	Camera *camera = new Camera();
	PhysicalCameraRig *camRig = new PhysicalCameraRig(*camera, *renderer);

    GLFWInput::getInstance().Init(renderer->window());
    //GLFWInput::getInstance().keyEvent.connect_member(camRig, &CameraRig::keyEvent);
    //GLFWInput::getInstance().mouseButtonEvent.connect_member(camRig, &CameraRig::mouseButtonEvent);
	GLFWInput::getInstance().mouseMoveEvent.connect_member(camRig, &PhysicalCameraRig::mouseMoveEvent);
         
    //set camera
    camera->name = "perspective";
    camera->up = glm::vec3(0.f, 1.f, 0.f);
	camera->target = glm::vec3(0.f, 0.f, -1.f);
    camera->position = glm::vec3(0.f, 0.f, 3.f);
    
    camera->FovY = 45.0f;
    camera->Aspect = ((float)WIDTH)/(float)HEIGHT;
    camera->ZNear = 0.01f;
    camera->ZFar = 100.0f;
    
    scene->loadedCamerasIDs.push_back(scene->Cameras.add(camera));
    scene->MainCameraID = scene->Cameras.query(&Camera::name, "perspective");
    
    //simple triangle
    std::vector<float> triangle_pos =
    {
        -0.8f, -0.8f, 0.0f,
        0.8f, -0.8f, 0.0f,
        0.0f, 0.8f, 0.0f
    };
    
    std::vector<unsigned int> triangle_indices =
    {
        0, 1, 2
    };
    
    std::vector<float> uv;
    std::vector<float> normals;
    //createMesh(*scene, "triangle", triangle_pos, uv, normals, triangle_indices, &wscene->loadedMeshIDs);
    loadMaterials(*scene, platform::exec_dir() + "/assets/head/head.mtl");
    loadMeshes(*scene, platform::exec_dir() + "/assets/head/head.obj");

    
    //load meshes
    //loadMeshes(*scene, platform::exec_dir() + "/assets/sponza/sponza.obj");
	//loadMaterials(*scene, platform::exec_dir() + "/assets/sponza/sponza.mtl");
	//loadMaterials(*scene, platform::exec_dir() + "/assets/sanMiguel/sanMiguel.mtl");
    
    //load shaders
	loadShader(*scene, platform::exec_dir() + "/shaders/vertex_shader.glsl", "vertex_shader", GL_VERTEX_SHADER);
	loadShader(*scene, platform::exec_dir() + "/shaders/pixel_shader.glsl", "pixel_shader", GL_FRAGMENT_SHADER);
    
    loadShaderProgram(*scene, "simple_shader",
                      scene->Shaders.query(&Shader::Name, "vertex_shader"),
                      scene->Shaders.query(&Shader::Name, "pixel_shader")
					  );
    
    //start render
	//make a queue of functions to be called inside game loop
	std::vector<std::function<void()>> functions_queue;
	functions_queue.push_back(std::bind(&PhysicalCameraRig::do_movements, camRig));
    functions_queue.push_back(std::bind(&GLRender::render, simpleRender, GLFWRenderer::frame_buffer_width, GLFWRenderer::frame_buffer_height));

	renderer->game_loop(functions_queue);
    
    return 0;
}
